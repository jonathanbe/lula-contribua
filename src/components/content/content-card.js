import './content-card.scss'

/*
 * Ease in-out quad transition
 * @param t - time
 * @param b - start value
 * @param c - change in value
 * @param d - duration
 */
let easeInOutQuad = (t, b, c, d) => {
	t /= d/2
	if (t < 1) return c/2*t*t + b
	t--
	return -c/2 * (t*(t-2) - 1) + b
}

$$('.ll-content-card').each(function() {
	let $card = $$(this),
		$buttons = $card.find('.ll-content-card__button')

	$buttons.each(function() {
		let $button = $$(this)

		$button.on('click', event => {
			let $target = $$($button.data('target')),
				element = document.scrollingElement,
				start = element.scrollTop,
				end = $target.offset().top + start - 50,
				duration = 500,
				increment = 20,
				time = 0

			// Scroll transition
			let animate = () => {
				time += increment
				element.scrollTop = easeInOutQuad(time, start, end - start, duration)

				if (time < duration)
					setTimeout(animate, increment)
			}

			// Call animate recursive function
			animate()
		})
	})
})
