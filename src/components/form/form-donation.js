import VMasker from 'vanilla-masker'
import './form-donation.scss'
import axios from 'axios'

$$('.ll-form-donation').each(function() {
	let $form = $$(this),
		$donation = $form.find('.ll-input-donation'),
		$tabs = $form.find('.ll-form-donation__tab'),
		$pessoa = $form.find('#donation-pessoa'),
		$cpf = $form.find('#donation-cpf'),
		$empresa = $form.find('#donation-empresa'),
		$cnpj = $form.find('#donation-cnpj'),
		$type = $form.find('#donation-type'),
		$submit = $form.find('#donation-submit'),
		$hidden = $form.find('.ll-input-donation__hidden'),
		$total = $form.find('.ll-form-donation__total'),
		monthly = false

	let validate = (update) => {
		let type = $type.val(),
			valid = false

		switch (type) {
			case 'pessoa-fisica':
				valid = ($pessoa[0].validate(update) && $cpf[0].validate(update))
				break;
			case 'pessoa-juridica':
				valid = $empresa[0].validate(update) && $cnpj[0].validate(update)
				break;
			default:
				break;
		}

		if ($hidden.val() == '' || $hidden.val() == 0)
			valid = false

		if (!$donation[0].validate())
			valid = false

		// Toggle submit button disabled
		// $submit.prop('disabled', !valid)

		return valid
	}

	$pessoa.on('keyup', event => validate())
	$cpf.on('keyup', event => validate())
	$empresa.on('keyup', event => validate())
	$cnpj.on('keyup', event => validate())

	$tabs.each(function() {
		let $tab = $$(this)

		$tab.on('click', event => {
			let $target = $$($tab.data('target')),
				$contents = $form.find('.ll-tab-content')

			// Toggle tab active class
			$tabs.removeClass('is-active')
			$tab.addClass('is-active')

			// Show/hide tab content
			$contents.hide()
			$target.show()

			// Toggle type
			if ($type.val() == 'pessoa-fisica')
				$type.val('pessoa-juridica')
			else
				$type.val('pessoa-fisica')

			// Then validate
			validate()

			// Prevent form submition
			event.preventDefault()
		})
	})

	var onFormSubmit = function(event) {
		if (!validate(true))
			return event.preventDefault()

		$submit.prop('disabled', true)

		axios({
			method: 'post',
			url: `${process.env.APP_URL}/donate`,
			data: {
				'donation-pessoa': $pessoa[0].val(),
				'donation-cpf': $cpf[0].val(),
				'donation-empresa': $empresa[0].val(),
				'donation-cnpj': $cnpj[0].val(),
				'type': $type.val(),
				'value': $hidden.val(),
				'monthly': monthly
			},
			responseType: 'json',
			headers: {
				'X-CSRF-TOKEN': $$('meta[name="csrf-token"]').attr('content')
			}
		})
			.then(response => {
				if (response.status == 200) {
					let donation = response.data.donation

					$form.find('input[name="ref_transacao"]').val(donation.id)
					$form.off('submit', onFormSubmit)
					$form.submit()
				}
			})
			.catch(error => {
				$submit.prop('disabled', false)
				console.error(error)
			})

		event.preventDefault()
	}

	$form.on('submit', onFormSubmit)

	// On true value change
	$hidden.on('change', event => {
		let value = VMasker.toMoney(event.detail, {
			zeroCents: true
		})

		// Change form total value
		$total.find('span:nth-child(1)').html(value)

		// Change pagseguro product value
		$form.find('input[name="item_valor_1"]').val(event.detail * 100)

		validate()
	})

	// On monthly checkbox change
	$donation.on('monthly-change', event => {
		let value = $hidden.val()
		monthly = event.detail

		if (value != 20 && value != 50 && value != 100 && value != 200 && value != 500 && value != 1000) {
			$total.find('span:nth-child(1)').html('0,00')
			$hidden.val('')
		}

		if (monthly) {
			$form.find('.ll-input-donation__input').val('')
			$form.prop('action', process.env.PAGSEGURO_PLAN)
		} else {
			$form.prop('action', process.env.PAGSEGURO_CHECKOUT)
		}

		validate()
	})

	$donation.on('code-change', event => {
		let code = event.detail

		$form.find('input[name="code"]').val(code)
	})

	// Set CSRF token
	$form.find('input[name="_token"]').val($$('meta[name="csrf-token"]').attr('content'))

	// Set initial form action
	$form.prop('action', process.env.PAGSEGURO_CHECKOUT)
})
