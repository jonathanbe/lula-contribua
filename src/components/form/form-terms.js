import './form-terms.scss'

$$('.ll-form-terms').each(function() {
	let $terms = $$(this),
		$close = $terms.find('.ll-form-terms__close'),
		$content = $terms.find('.ll-form-terms__content')

	$terms[0].open = function() {
		document.body.style.overflow = 'hidden';
		$terms.addClass('is-active')
	}

	$terms[0].close = function() {
		document.body.style.overflow = null;
		$terms.removeClass('is-active')
	}

	$terms[0].toggle = function() {
		if ($terms.hasClass('is-active'))
			$terms[0].close()
		else
			$terms[0].open()
	}

	$content.on('click', event => {
		event.stopPropagation()
	})

	$terms.on('click', event => {
		$terms[0].close()
	})

	$close.on('click', event => {
		$terms[0].close()
		event.preventDefault()
	})

	setTimeout(() => $terms.css('display', 'block'), 500)
})
