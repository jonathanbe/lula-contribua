import './form-callback.scss'

$$('.ll-form-callback').each(function() {
	let $callback = $$(this),
		$close = $callback.find('.ll-form-callback__close'),
		$content = $callback.find('.ll-form-callback__content')

	$callback[0].open = function() {
		document.body.style.overflow = 'hidden';
		$callback.addClass('is-active')
	}

	$callback[0].close = function() {
		document.body.style.overflow = null;
		$callback.removeClass('is-active')
	}

	$callback[0].toggle = function() {
		if ($callback.hasClass('is-active'))
			$callback[0].close()
		else
			$callback[0].open()
	}

	$content.on('click', event => {
		event.stopPropagation()
	})

	$callback.on('click', event => {
		$callback[0].close()
	})

	$close.on('click', event => {
		$callback[0].close()
		event.preventDefault()
	})

	setTimeout(() => $callback.css('display', 'block'), 500)
})
