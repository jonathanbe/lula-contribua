import VMasker from 'vanilla-masker'
import './input-text.scss'

$$('.ll-input-text').each(function() {
	let $text = $$(this),
		$input = $text.find('.ll-input-text__input'),
		$error = $text.find('.ll-input-text__error'),
		error = null,
		mask = $text.data('mask')

	switch (mask) {
		case 'cpf':
			VMasker($input).maskPattern('999.999.999-99')
			break;
		case 'cnpj':
			VMasker($input).maskPattern('99.999.999/9999-99')
			break;
		default:
			break;
	}

	$text[0].validate = function(update) {
		let value = $input.val(),
			valid = false

		if (mask == 'cpf') {
			valid = validateCPF(value)
			error = 'Número de CPF inválido'
		} else if (mask == 'cnpj') {
			valid = validateCNPJ(value)
			error = 'Número de CNPJ inválido'
		} else {
			valid = value.length > 0
			error = 'Campo obrigatório'
		}

		if (update)
			$text[0].update()

		return valid
	}

	$text[0].val = function() {
		return $input.val()
	}

	$text[0].update = function() {
		let valid = $text[0].validate()

		if (valid) {
			error = null
			$text.removeClass('is-invalid')
		}

		if (error) {
			$error.html(error)
			$text.addClass('is-invalid')
		}
	}

	$input.on('focus', event => $text.trigger('focus', event))
	$input.on('blur', event => $text.trigger('blur', event))
	$text.on('keyup', event => $text[0].update())
})

function validateCPF(c) {
	if((c = c.replace(/[^\d]/g,"")).length != 11)
		return false;
	if (c == "00000000000")
		return false;
	var r;
	var s = 0;
	for (var i=1; i<=9; i++)
		s = s + parseInt(c[i-1]) * (11 - i); 
	r = (s * 10) % 11;
	if ((r == 10) || (r == 11)) 
		r = 0;
	if (r != parseInt(c[9]))
		return false;
	s = 0;
	for (var i = 1; i <= 10; i++)
		s = s + parseInt(c[i-1]) * (12 - i);
	r = (s * 10) % 11;
	if ((r == 10) || (r == 11)) 
		r = 0;
	if (r != parseInt(c[10]))
		return false;
	return true;
}

function validateCNPJ(c) {
	var b = [6,5,4,3,2,9,8,7,6,5,4,3,2];
	if((c = c.replace(/[^\d]/g,"")).length != 14)
		return false;
	if(/0{14}/.test(c))
		return false;
	for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]);
	if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
		return false;
	for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]);
	if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
		return false;
	return true;
}

