import './input-checkbox.scss'

$$('.ll-input-checkbox').each(function() {
	let $checkbox = $$(this),
		$input = $checkbox.find('.ll-input-checkbox__input')

	$input.on('change', event => {
		$checkbox.toggleClass('is-active')
		$checkbox.trigger('change', event)

		event.stopPropagation()
	})
})
