import VMasker from 'vanilla-masker'
import './input-donation.scss'

$$('.ll-input-donation').each(function() {
	let $donation = $$(this),
		$input = $donation.find('.ll-input-donation__input'),
		$switch = $donation.find('.ll-input-donation__switch'),
		$buttons = $donation.find('.ll-input-donation__button'),
		$hidden = $donation.find('.ll-input-donation__hidden')

	VMasker($input).maskMoney({
		precision: 0,
		separator: ',',
		delimiter: '.',
		unit: 'R$',
		zeroCents: false
	})

	$input.on('blur', event => {
		let value = VMasker.toNumber($input.val())

		$hidden.val(value)
		$hidden.trigger('change', $hidden.val())
		$buttons.removeClass('is-active')
	})

	$switch.on('change', event => {
		$$('.ll-app').toggleClass('ll-app--primary')
		$$('.ll-app').toggleClass('ll-app--secondary')

		let isMonthly = $$('.ll-app').hasClass('ll-app--secondary')

		// $buttons.removeClass('is-active')
		$input.prop('disabled', isMonthly)
		$donation.trigger('monthly-change', isMonthly)
		$input.prop('placeholder', isMonthly ? 'Selecione um valor acima' : 'Ou digite outro valor')
		$donation[0].validate()
	})

	$buttons.each(function() {
		let $button = $$(this),
			value = $button.data('value'),
			code = $button.data('code')

		$button.on('click', event => {
			$buttons.removeClass('is-active')
			$button.addClass('is-active')

			$input.val(null)
			$hidden.val(value)
			$donation.trigger('code-change', code)
			$hidden.trigger('change', $hidden.val())

			event.preventDefault()
		})
	})

	$donation[0].validate = function() {
		let isMonthly = $$('.ll-app').hasClass('ll-app--secondary')
		let isButtonActive = () => {
			let active = false

			$buttons.each(function() {
				if ($$(this).hasClass('is-active'))
					active = true
			})

			return active
		}

		if (isMonthly && !isButtonActive() ||
			!isMonthly && $input.val().length == 0 && !isButtonActive()
		) {
			$donation.addClass('is-invalid')
			return false
		}

		$donation.removeClass('is-invalid')
		return true
	}
})
