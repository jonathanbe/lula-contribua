import './app.scss'

$$('.ll-app').each(function() {
	let $app = $$(this),
		$window = $$(window),
		$fixed = $app.find('.ll-app__fixed'),
		offset = $fixed.offset().top,
		$terms = $app.find('.ll-form-terms'),
		$termsLink = $app.find('.ll-app__terms-link')

	$window.on('scroll', event => {
		let scroll = document.scrollingElement.scrollTop

		if (scroll >= offset)
			$fixed.addClass('is-active')
		else
			$fixed.removeClass('is-active')
	})

	$termsLink.on('click', event => {
		$terms[0].open()
		event.preventDefault()
	})
})
