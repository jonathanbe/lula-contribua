document.addEventListener("DOMContentLoaded", function() {
	var template = require('@/app.hbs'),
		path = window.location.pathname

	// Append app template to body
	document.body.innerHTML += template({
		path: path,
		pagseguro: {
			email: process.env.PAGSEGURO_EMAIL,
			checkout: process.env.PAGSEGURO_CHECKOUT,
			plan: process.env.PAGSEGURO_PLAN,
		},
		plans: {
			plan_20: process.env.PLAN_20,
			plan_50: process.env.PLAN_50,
			plan_100: process.env.PLAN_100,
			plan_200: process.env.PLAN_200,
			plan_500: process.env.PLAN_500,
			plan_1000: process.env.PLAN_1000,
		}
	})

	if (path == '/callback')
		$$('.ll-form-callback').addClass('is-active')

	// Initialize application
	require('@/app')
	require('@/components')
});

