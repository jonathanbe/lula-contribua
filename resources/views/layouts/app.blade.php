<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="Contribua com o Instituto Lula" />
		<meta property="og:title" content="Contribua com o Instituto Lula" />
		<meta property="og:description" content="Vamos juntos manter o Instituto Lula em atividade em 2018." />
		<meta property="og:image" content="https://lula-contribua.herokuapp.com/assets/images/social_share.jpg" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Instituto Lula</title>
	</head>
	<body>
		<script type="text/javascript" src="/main.bundle.js"></script>
	</body>
</html>
