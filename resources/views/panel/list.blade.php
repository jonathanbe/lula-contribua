@extends('layouts.panel')

@section('content')

<div class="donation">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="/panel">
					Instituto Lula – Doações
				</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="/logout">
						Logout
					</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="panel panel-default">
		<div class="panel-body">
			<form action="/search" method="GET" class="donation__search form-inline">
				@if (!@$keyword)
					<span class="donation__result">
						Exibindo <b>{{ $count }}</b> registro(s):
					</span>
				@else
					<span class="donation__result">
						Exibindo <b>{{ $count }}</b> registro(s) com a palavra-chave <b>{{ $keyword }}</b>:
					</span>
				@endif
                <div class="checkbox">
                <label>
                  <input type="checkbox" name="show_all" @if (@$show_all == 1) checked="checked" @endif value="1">
                    Mostrar todos
                </label>
              </div>
				<div class="form-group">
					<input type="text" class="form-control" name="q" id="search" placeholder="Palavra-chave" value="{{ @$keyword }}">
				</div>
				<button type="submit" class="btn btn-default">Buscar</button>
			</form>
		</div>

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Entidade</th>
					<th>CPF/CNPJ</th>
					<th>Tipo</th>
					<th>Valor</th>
					<th>Assinatura Mensal</th>
					<th>Gerado em</th>
					<th>Código da Transação</th>
				</tr>
			</thead>

			@foreach($data as $d)
				<tr>
					<td>{{ $d->name }}</td>
					<td>{{ $d->document }}</td>
					<td>{{ $d->type == 'pessoa-fisica' ? 'Pessoa Física' : 'Pessoa Jurídica' }}</td>
                    <td>{{ $d->value }}</td>
					<td>{{ $d->monthly == 1 ? 'Sim' : 'Não' }}</td>
					<td>{{ strftime('%d/%m/%Y', strtotime($d->created_at)) }}</td>
					<td>{{ !$d->code ? 'N/D' : $d->code }}</td>
				</tr>
			@endforeach
		</table>
	</div>

	<!--nav>
		<ul class="pagination">
			<li>
				<a href="#" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li>
				<a href="#" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</ul>
	</nav-->
</div>
@endsection
