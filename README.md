# Lula Contribua – Documentação

## Dependências de desenvolvimento
- `node`
- `webpack`
- `dom7`
- `sass`
- `handlebars`

## Inicializando
Para inicialiazar o projeto, é necessário instalar as dependências do `node` utilizando o comando abaixo:
`npm install`

## Desenvolvendo
Para modificar o frontend, utilize o comando:
`npm start:dev`

Para visualiza-lo acesse no seu navegador o endereço: http://localhost:8080

_PS: Para modificar o `host` e `port` do servidor de desenvolvimento, altere o arquivo `.env`_

## Estrutura
Todo o código do projeto está localizando dentro da pasta `src`.

### Componentes
O projeto é constituído apenas de componentes, o mais importante para a implementação da lógica de Backend é o:
`components/form/form-donation`

Responsável pelo formulário de doação presente na lateral da página. Aqui são definido os inputs e action do formulário.

### Tecnologias
- Estamos utilizando o motor de template **handlebars** para as views (extensão `*.hbs`).
- Para estilo, o **sass** (extensão `*.scss`)
- Com o `babel` temos uma sintaxe de scripts moderna (extensão `*.js`)

## Implementando Backend
O formulário de doação está submetendo um `POST` para a URL `/` (raiz do dominio), os paramêtros:

- `donation-pessoa` para nome, quando pessoa pessoa física for selecionado.
- `donation-cpf` para documento, quando pessoa física for selecionado.
- `donation-empresa` para nome, quando pessoa jurídica for selecionado.
- `donation-cnpj` para documento, quando pessoa jurídica for selecionado.
- `type` terá o valor **pessoa-fisica** ou **pessoa-juridica**, dependendo da opção selecionada.
- `value` para valor em reais da doação.
- `monthly` será **boolean**, para indicar se a doação acontece mensalmente ou não.

## Deployment
Para compilar scripts e estilos, utilize o comando:
`npm run build`
