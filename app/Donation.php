<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
   public $timestamps = true;
   protected $fillable = [
        'name', 'document', 'type', 'code', 'value', 'monthly',
    ];
   
   
   /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('name', "LIKE", "%$q%")
            ->orWhere("document", "LIKE", "%$q%")
            ->orWhere("code", "LIKE", "%$q%");
    }
}
