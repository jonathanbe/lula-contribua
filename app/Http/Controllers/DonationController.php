<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('layouts.app');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input = $request->all();

        // Check donation type
        if ($input['type'] == 'pessoa-fisica')
            $document = $input['donation-cpf'];
        else
            $document = $input['donation-cnpj'];

        // Donation data
        $data = [
            'name' => $input['donation-pessoa'],
            'document' => $document,
            'type' => $input['type'],
            'value'  => $input['value'],
            'monthly'  => $input['monthly']
        ];

        // Create donation and session
        $donation = Donation::create($data);
        $request->session()->put('donation', $donation->id);

        // Return created donation
        return response([
            'donation' => $donation
        ], 200);
    }
    
    public function notification(Request $request){
        $input = $request->all();

        // Initialize PagSeguro
        \PagSeguro\Library::initialize();
        \PagSeguro\Configuration\Configure::setEnvironment(env('PAGSEGURO_ENV', 'sandbox'));

        try {
            $credentials = new \PagSeguro\Domains\AccountCredentials(env('PAGSEGURO_EMAIL'), env('PAGSEGURO_TOKEN'));
            $response = \PagSeguro\Services\Transactions\Notification::check($credentials);

            $donation = Donation::find($response->getReference());
            $donation->code = $response->getCode();
            $donation->save();
        } catch (Exception $e) {
            die($e->getMessage());
        }

        return response('success', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $input = $request->all();

        if ($request->session()->has('donation')) {
            if(isset($input['code'])) {
                // Retrieve and delete donation id
                $id = $request->session('donation')->pull('donation');

                // Find and update donation
                $donation = Donation::find($id);

                if ($donation)
                    $donation->update(['code' => $input['code']]);
            }
        }

        return view('layouts.app');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
