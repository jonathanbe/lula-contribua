<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;

class PanelController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$donations = Donation::whereNotNull('code')->get();

		return view('panel.list', [
			'data' => $donations,
			'show_all' => 0,
			'count' => $donations->count()
		]);
	}

	/**
	 * Search Donations
	 * @param Request $request
	 * @return type
	 */
	public function search(Request $request)
	{
		$input = $request->all();
		$show_all = isset($input['show_all']) ? 1 : 0;

		try {
			$donations = Donation::search($input['q']);

			if (!$show_all)
				$donations = $donations->whereNotNull('code');

			$donations = $donations->get();
		} catch (\Exception $e) {
			$e->getMessage();
			return response($e->getMessage(), 400);
		}

		return view('panel.list', [
			'data' => $donations,
			'count' => $donations->count(),
			'show_all' => $show_all,
			'keyword' => $input['q']
		]);
	}

	/**
	 * Create a PagSeguro plan
	 * @param Request $request
	 */
	public function plan(Request $request, $value)
	{
		$input = $request->all();

		// Initialize PagSeguro
		\PagSeguro\Library::initialize();
		\PagSeguro\Configuration\Configure::setEnvironment(env('PAGSEGURO_ENV', 'sandbox'));

		$plan = new \PagSeguro\Domains\Requests\DirectPreApproval\Plan();
		$plan->setRedirectURL('https://lula-contribua.herokuapp.com/callback');
		$plan->setPreApproval()->setName('Plano R$ ' . $value);
		$plan->setPreApproval()->setCharge('AUTO');
		$plan->setPreApproval()->setPeriod('MONTHLY');
		$plan->setPreApproval()->setAmountPerPayment($value);
		$plan->setPreApproval()->setDetails('Plano mensal de R$ ' . $value . '.');
		$plan->setPreApproval()->setCancelURL("https://lula-contribua.herokuapp.com/cancel");
		$plan->setReviewURL('https://lula-contribua.herokuapp.com/review');
		$plan->setReceiver()->withParameters(env('PAGSEGURO_EMAIL'));

		try {
			$response = $plan->register(
				new \PagSeguro\Domains\AccountCredentials(env('PAGSEGURO_EMAIL'), env('PAGSEGURO_TOKEN'))
			);
		} catch (\Exception $e) {
			$e->getMessage();
			return response($e->getMessage(), 400);
		}

		return response([
			'plan' => $plan
		], 200);
	}
}
