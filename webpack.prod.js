var path = require('path');
var webpack = require('webpack');
var merge = require('webpack-merge');
var baseWebpackConfig = require('./webpack.config');

module.exports = merge(baseWebpackConfig, {
	devtool: 'cheap-source-map',
	output: {
		path: path.join(__dirname, 'public'),
		publicPath: '/',
		filename: '[name].bundle.js',
	}
})
