var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')

// Config dotenv
require('dotenv').config()

module.exports = {
	entry: './src/main.js',
	resolve: {
		extensions: ['.js'],
		alias: {
			'@': path.join(__dirname, 'src'),
			'~': path.join(__dirname, 'node_modules'),
			'Dom7': 'dom7/dist/dom7.js',
			'handlebars': 'handlebars/dist/handlebars.min.js'
		}
	},
	devServer: {
		contentBase: path.join(__dirname, 'public'),
		compress: true,
		port: 8080,
		host: '0.0.0.0',
		disableHostCheck: true,
		historyApiFallback: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules'
			},
			{
				test: /\.hbs$/,
				loader: 'handlebars-loader',
				query: {
					partialDirs: [
						path.join(__dirname, 'src', 'components')
					],
					helperDirs: [
						path.join(__dirname, 'src', 'helpers')
					]
				},
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							data: "@import '~@/stylesheets/main.scss';",
							includePaths: [
								require('bourbon-neat').includePaths,
							]
						}
					}
				]
			},
		]
	},
	plugins: [
		new webpack.EnvironmentPlugin({
			NODE_ENV: 'development',
			APP_URL: '',
			PAGSEGURO_EMAIL: '',
			PAGSEGURO_CHECKOUT: '',
			PAGSEGURO_PLAN: '',
			PLAN_20: '',
			PLAN_50: '',
			PLAN_100: '',
			PLAN_200: '',
			PLAN_500: '',
			PLAN_1000: '',
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'index.html',
			title: 'Web',
			inject: true,
		}),
		new webpack.ProvidePlugin({
			$$: 'Dom7',
			Dom7: 'Dom7',
			'window.Dom7': 'Dom7'
		})
	]
}
